

  ## 1. हाल ही में दिल्ली से जयपुर तक भारत की पहली हाइड्रोजन बस सेवा की शुरुआत की जाएगी 
![Image result for first hydrogen bus in india](https://img.fuelcellsworks.com/wp-content/uploads/2020/04/NTPC-Hydrogen.jpg)

  ## 2. हाल ही में केरल में भारत का पहला डिजिटल विश्वविद्यालय स्थापित किया जायेगा 
![Image result for kerala map](https://upload.wikimedia.org/wikipedia/commons/2/2f/Administrative_Divisions_of_Kerala_%282020%29.svg)

  ## 3. हाल ही में मध्य प्रदेश सरकार ने होशंगाबाद शहर का नाम बदलकर नर्मदापुरम रखने की घोषणा की है
![Image result for madhya pradesh map](https://i.pinimg.com/originals/7f/04/42/7f0442a09b46551be476635a10437173.jpg)

  ## 4. हाल ही में पंजाब में 'जल है तो कल है(एक साप्ताहिक कार्यक्रम)' प्रसारित किया जा रहा है
![Image result for jal hai to kal hai](https://www.1hindi.com/wp-content/uploads/2020/08/jal-hai-to-kal-hai-nibandh.jpg)

