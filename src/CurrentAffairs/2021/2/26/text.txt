

  ## 1. हाल ही में हमारे गृह मंत्री ने 'राष्ट्र प्रथम-82 वर्षो की स्वर्णिम गाथा' नामक पुस्तक का विमोचन किया है, यह पुस्तक CRPF से सम्बंधित है 
![गृह मंत्रालय केंद्रीय गृह मंत्री श्री अमित शाह ने सीआरपीएफ की ' राष्ट्र  प्रथम - 82 वर्षों की स्‍वर्णिम गाथा' पुस्तक का विमोचन किया यह ...](https://static.pib.gov.in/WriteReadData/userfiles/image/image001WK7Q.jpg)

  ## 2. हाल ही में कुशीनगर हवाईअड्ढा उत्तर प्रदेश का 3रा अंतराष्ट्रीय हवाईअड्ढा बन गया है 
![International Flight Operations To Begin At Kushinagar Airport By Diwali |  Curly Tales](https://curlytales.com/wp-content/uploads/2020/09/photo-1542296332-2e4473faf563-2-2-1170x614.jpg)

  ## 3. हाल ही में रामनाथ कोविंद(राष्ट्रपति) ने 'मोटेरा स्टेडियम गुजरात' का उद्घाटन किया है
![India vs England 3rd Test: Motera Cricket Stadium, World's Biggest, to be  Renamed as Narendra Modi Stadium](https://images.news18.com/ibnlive/uploads/2021/02/1612783236_motera-stadium1.jpg)

