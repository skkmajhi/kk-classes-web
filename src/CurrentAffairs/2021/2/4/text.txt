

  ## 1. हाल ही में भव्या लाल को NASA के कार्यवाहक प्रमुख के रूप में नियुक्त किया गया है 
![Bhavya Lal: जानिए कौन हैं भव्या लाल, जिन्हें अमेरिकी स्पेस एजेंसी (NASA)  में कार्यकारी प्रमुख बनाया गया है](https://bansalnews.com/wp-content/uploads/2021/02/bhawaya-lal.jpg)

  ## 2. बजट 2021 के अनुसार भारत में कुल 7 मेगा टेक्सटाइल पार्क स्थापित किए जायेंगे 
![Green nod for Mega Textile Park in Warangal - The Hindu](https://www.thehindu.com/news/cities/Hyderabad/article19310618.ece/ALTERNATES/LANDSCAPE_1200/hy19textile)

  ## 3. बजट 2021 के अनुसार 'डीप ओशन मिशन' के लिए 4000 करोड़ रुपए आवंटित किये गए है 
![Explained: India's Deep Ocean Mission – Civilsdaily](https://www.thehindu.com/sci-tech/science/p0ali3/article28809028.ece/alternates/FREE_615/TH04Deep-sea-miningcol)



