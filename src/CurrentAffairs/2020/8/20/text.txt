

  ## 1. हाल ही में सत्यपाल मालिक को मेघालय राज्य का नया राज्यपाल नियुक्त किया गया है 
![Coronavirus: Cases Surge In North-East; Assam Reports 44 Patients ...](https://images.assettype.com/swarajya/2020-05/0d10eb2c-2aa1-49df-85bb-205f38ebcc23/600px_Northeast_india.png?w=1200&h=900)

  ## 2. ग्वालियर-चम्बल एक्सप्रेसवे का नाम अटल बिहारी के नाम पर रखा गया है
![मोदी सरकार ने किया चंबल एक्सप्रेस-वे ...](https://smedia2.intoday.in/aajtak/images/stories/092019/express_way.jpg_1593849216_618x347.jpeg)

  ## 3. पंडित जसराज का निधन हुआ है ये एक गायक थे 
![Pandit Jasraj, Indian classical music legend, dies at 90 - CNN](https://dynaimage.cdn.cnn.com/cnn/c_fill,g_auto,w_1200,h_675,ar_16:9/https%3A%2F%2Fcdn.cnn.com%2Fcnnnext%2Fdam%2Fassets%2F200817121514-restricted-file-pandit-jasraj-2019.jpg)

  ## 4. राकेश अस्थाना BSF के नए DGP बने है
![CBI controversy: Rakesh Asthana's journey from supercop in Gujarat ...](https://s01.sgp1.digitaloceanspaces.com/large/899669-46607-jqvtmbrlzu-1480678657.jpeg)
 


 # अगस्त के महत्तवपूर्ण दिन
 ### 09= **भारत छोड़ो दिवस-आदिवासी दिवस**
 ### 12= **अंतर्राष्ट्रीय युवा दिवस(2000)**
 ### 29= **राष्ट्रीय खेल दिवस**

 ___
 # आज का सवाल 

 ### **आनंद मठ के लेखक कौन है ?**
 ![Anand Math | (1952) Patriotic Historical Movie | आनंद मठ ...](https://i.ytimg.com/vi/dSR3nDzvECc/maxresdefault.jpg)