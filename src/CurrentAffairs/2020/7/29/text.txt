

 ## 1. हाल ही में मालदीव्स देश ने 26 जुलाई को अपना 55वा स्वतंत्रता दिवस मनाया है
![Maldives Map | Tourist informations | Best guest house in Maldives ...](https://www.huraa-island.com/wp-content/gallery/maldives-map/Maldives-map.jpg)

 ## 2. हाल ही में बांग्लादेश के क्रिकेटर काजी अनिक इस्लाम पर 2 वर्ष का प्रतिबन्ध लगाया गया है 
 ![Oxfam to help over 200,000 Rohingya in Bangladesh | Oxfam](https://assets.oxfamamerica.org/media/images/Bangladesh-map-09-2017-aa_VK.2e16d0ba.fill-1180x738-c100.png)

 ___
 # जुलाई के महत्तवपूर्ण दिन
 ### 01= **SBI स्थापना/डॉक्टर्स/GST दिवस**
 ### 04= **अमेरिका स्वतंत्रता दिवस(1776)**
 ### 11= **विश्व जनसँख्या दिवस**
 ### 18= **नेल्सन मंडेला अंतर्राष्ट्रीय दिवस**
 ###  26= **कारगिल विजय दिवस**
 ###  29= **विश्व बाघ दिवस(1973)**

 ___
 # आज का सवाल 

 ### **संगम युग में उरैयूर किसलिए विख्यात था ?**
![Google Image Result for http://joeybonifacio.com/wp-content ...](https://i.pinimg.com/originals/2e/a4/1a/2ea41aad0a0fbb4cb72b7469cdd9ac74.jpg)