

  ## 1. ICC ने अंतराष्ट्रीय क्रिकेट खिलाड़ियों के लिए कम से कम 15 वर्ष की उम्र निर्धारित किया है 
  ![ICC postpones two series on road to World Cup 2023 due to ongoing COVID-19  pandemic](https://cdn.dnaindia.com/sites/default/files/styles/full/public/2020/06/11/909107-907073-afp.jpg)
  

  ## 2. हाल ही में प्रेम प्रकाश ने रिपोर्टिंग इंडिया नामक पुस्तक लिखी है
  ![Reporting India: My Seventy-year Journey as a Journalist: Amazon.co.uk:  Prakash, Prem: 9780670093984: Books](https://images-na.ssl-images-amazon.com/images/I/51AwhSVWuyL._SX319_BO1,204,203,200_.jpg)
  
 ___
 # आज का सवाल 

 ### **प्रेयरी घास के मैदान कहा पर हैै ?**
![What Is a Grassland Ecosystem?](https://images.reference.com/amg-cms-reference-images/media/grassland-ecosystem_fb01373ca43f56d8.jpg)