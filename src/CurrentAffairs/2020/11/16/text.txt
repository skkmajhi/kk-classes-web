

  ## 1. हाल ही में सहादत रेहमान को अंतराष्ट्रीय बाल शांति पुरस्कार से सम्मानित किया गया है
![Epaar Bangla, opaar Bangla | Dhaka Tribune](https://media-eng.dhakatribune.com/uploads/2019/06/partha-bigstock-1560767749247.jpg)

  ## 2. हाल ही में वस्त्र मंत्रालय ने '#लोकल4दिवाली' अभियान की शुरुआत की है
![Ministry of Textile - Schemes and Programs | Study Wrap](https://studywrap.com/wp-content/uploads/2020/02/Ministry-of-Textile-scaled.jpg)

  ## 3. हाल ही में अभिजीता गुप्ता को विश्व के सबसे कम उम्र के लेखक के रूप में चुना गया है
![Gurgaon-based 7-year-old, Abhijita Gupta, Becomes the World's Youngest  Author](https://s.yimg.com/uu/api/res/1.2/CjTZSj3bSKUXOhn5PJgXsg--~B/aD03OTU7dz0xMjAwO3NtPTE7YXBwaWQ9eXRhY2h5b24-/https://media-mbst-pub-ue1.s3.amazonaws.com/creatr-uploaded-images/2020-11/6ba422e0-1ecd-11eb-bebf-c2fad340f6ab)

  ## 4. हाल ही में अयोध्या ने दीपो के सबसे बड़े प्रदर्शन के लिए गिंनीस वर्ल्ड रिकॉर्ड बनाया है
![ayodhya online viewing of diwali festival website crashes up govts website  celebrate virtual diwali ayodhya ki diwali ka video and photo amh | Ayodhya  Diwali Video and Photo : अयोध्या की दिवाली](https://gumlet.assettype.com/Prabhatkhabar%2F2020-11%2Ff328b786-dd2c-4565-bc4f-5971906f99b1%2FEmt5Cf4UUAYVQGO.jpg?format=webp&w=400&dpr=2.6)

 ___
 # आज का सवाल 

 ### **आस्वान बांध किस नदी पर स्थित है ?**
![Aswan High Dam | High Dam Aswan | Aswan Dam](https://www.privatetoursinegypt.com/uploads/High-Dam.jpg)