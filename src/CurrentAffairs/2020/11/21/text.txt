

  ## 1. हाल ही में रिचा चड्डा(अभिनेत्री) को भारत रत्न डॉ भीमराव आंबेडकर पुरस्कार से सम्मानित किया गया है
![Defamation suit: Actor tenders apology to Richa Chadha, HC disposes of suit  | Cities News,The Indian Express](https://images.indianexpress.com/2020/10/richa.jpg)

  ## 2. हाल ही में FIFA ने भारत देश में होने वाले अंडर-17 महिला विश्व कप को स्थगित कर दिया है
![India Bordering Countries - QuickGS.com](https://lh3.googleusercontent.com/proxy/eDNbLlA3Q2NvhIGCP95ImSi3I6r9ZISobzq5ziE0nRU4cRjM_2U5hY3ABUxcFFA0mxdoctsVhV4FzwzA2YMvdRLUYUQPXWb4Y-Om_iiSWP5SJUHrvx4511wkrJwTpXGbig)

  ## 3. हाल ही में चित्रा बनर्जी ने 'The Last Queen' नामक पुस्तक लिखी है
![Queen of Dreams by Chitra Banerjee Divakaruni: 9781400030446 |  PenguinRandomHouse.com: Books](https://images2.penguinrandomhouse.com/cover/9780307427397)

  ## 4. हाल ही में बराक ओबामा(अमेरिका के पूर्व राष्ट्रपति) ने 'A Promised Land' नामक पुस्तक लिखी है 
![Barack Obama book 'A Promised Land' sells 887K copies on first day](https://www.gannett-cdn.com/presto/2020/11/13/USAT/8f1b4506-f147-41ae-afbc-bec29cbf37e5-A_Promised_land.jpg?width=300&height=457&fit=crop&format=pjpg&auto=webp)

 ___
 # आज का सवाल 

 ### **सापेक्षित आद्रता के मापन हेतु किस उपकरण का प्रयोग किया जाता है ?**
![File:Saussure's Hygrometer diagram.png - Wikimedia Commons](https://upload.wikimedia.org/wikipedia/commons/f/fb/Saussure%27s_Hygrometer_diagram.png)