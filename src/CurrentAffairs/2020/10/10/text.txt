

  ## 1. 8 अक्टूबर को हमने भारतीय वायुसेना दिवस मनाया
![88th Anniversary of Indian Air Force Day - YouTube](https://i.ytimg.com/vi/fUtzt2dQlpI/maxresdefault.jpg)

  ## 2. हाल ही में रसायन विज्ञान के छेत्र का नोबेल पुरस्कार वैज्ञानिक इमैनुएल चारपीयर और जेनिफर डुडना को देने की घोषणा की है
![2020 Nobel Prize in Chemistry awarded to Emmanuelle Charpentier and  Jennifer A. Doudna for development of a method for genome editing - world  news - Hindustan Times](https://m.hindustantimes.com/rf/image_size_444x250/HT/p2/2020/10/07/Pictures/files-sweden-nobel-chemistry_d6b65568-0883-11eb-bb5c-c76064722a88.jpg)

  ## 3. भारत ने जापान देश के साथ साइबर सुरक्षा के छेत्र में समझौता किया है
![US sanctions on Iran to affect Turkey, India most || AW](https://thearabweekly.com/sites/default/files/2019-05/Iran_map_AFP.jpg)

 ___
 # आज का सवाल 

 ### **पहली उद्योगिक क्रांति किस देश में आयी ?**
![Industrial Revolution: Definitions, Causes & Inventions - HISTORY](https://imageproxy.themaven.net//https%3A%2F%2Fwww.history.com%2F.image%2FMTYxNTEyNTkxOTg0Njk5MDM1%2Fsecond_industrial_revolution_gettyimages-51632462.jpg)
