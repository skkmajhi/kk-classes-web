

  ## 1. हाल ही में दुर्गापुर पश्चिम बंगाल में दुनिया का सबसे बड़ा Solar Tree स्थापित किया गया है
![World's largest Solar Tree installed at Durgapur; customized for use in  Agri-Operations - Newslux](https://newslux.in/wp-content/uploads/2020/09/image004QGLG.jpg)

  ## 2. प्रधानमंत्री नरेंद्र मोदी ने सितम्बर महीने को पोषण माह के रूप में मनाये जाने की घोषणा की है
![How to find the right nutrition advice for a healthy gut?](https://www.gutmicrobiotaforhealth.com/wp-content/uploads/2020/05/gut-health-4-myths-nutrition-scaled.jpg)

  ## 3. हाल ही में असम में बच्चो के लिए पहला अखबार The Young Minds लांच किया गया है
![Northeast India - Wikipedia](https://upload.wikimedia.org/wikipedia/commons/9/90/Northeast_india.png)

  ## 4. हाल ही में भारत ने 2030 वर्ष तक 100 मिलियन टन कोयले के गैसियकारन का लक्ष्य रखा है 
![Lockdown Delays Commercial Coal Mining Auction To July](https://odishatv.in/wp-content/uploads/2020/05/Coal-Mining.jpg)

 ___
 # आज का सवाल 

 ### **भारतीय संसद के कितने अंग होते है ?**
 ![Plastic out, bottles from home back as Parliament goes green - india news -  Hindustan Times](https://m.hindustantimes.com/rf/image_size_444x250/HT/p2/2019/09/25/Pictures/parliament-indian-parliament-building-session-pictured-opening_709b6c58-df00-11e9-b0cd-667d8786d605.jpg)