

  ## 1. हाल ही में खेल मंत्रालय ने 'योगासन' को प्रतिस्प्रधा खेल के रूप में मान्यता दी है 
![Government Formally Recognises Yogasana As Competitive Sport](https://www.gonewsindia.com/assets/uploads/yogasana_sport_17122020.jpg)

  ## 2. हाल ही में राजगीर में बिहार का पहला 'गिलास ब्रिज' बनाया गया है
![Not China, Bihar is sky-walk ready; set foot on 200-feet glass bridge any  time next year - The Financial Express](https://images.financialexpress.com/2020/12/rajgir-glass-bridge.jpeg)

  ## 3. ब्रिटैन 2021 में G7 देशो की बैठक की अध्यक्षता करेगा 
![Federal Ministry of Finance - Group of Seven/Group of Eight (G7/G8)](https://www.bundesfinanzministerium.de/Content/EN/Bilder/Teaser/G7-Members.jpg?__blob=normal&v=3)

  ## 4. हाल ही में जयराम रमेश ने 'The Light Of Asia' नामक कविता लिखी है
![The Light of Asia (Annotated) eBook: Arnold, Edwin, Inguglia, Vito,  Meighan, Paul: Amazon.in: Kindle Store](https://m.media-amazon.com/images/I/51B3SxmO1pL.jpg)

  ## 5. हाल ही में रिकार्डो रिको(साइकिलिंग के खिलाडी) पर आजीवन प्रतिबन्ध लगा दिया गया है 
![Marga Rico and the sport of triathlon | Triathlon.org](https://www.triathlon.org/uploads/webgalleries/127488/elite-women-28.jpg)


 ___
 # आज का सवाल 

 ### **भारत का प्रथम परमाणु ऊर्जा उत्पादन केंद्र कौन सा है ?**
![भारत में परमाणु ऊर्जा - विकिपीडिया](https://upload.wikimedia.org/wikipedia/commons/7/7e/Kudankulam_NPP.jpg)