

  ## 1. हाल ही में उतपाल कुमार सिंह को लोक सभा का नया महासचिव नियुक्त किया गया है
![मोदी को पसंद आई उत्तराखंड के पूर्व मुख्य सचिव उत्पल कुमार सिंह की शैली,  बनाए गए लोकसभा के महासचिव | IAS Utpal Kumar Singh appointed as General  Secretary of Lok sabha kpa](https://static.asianetnews.com/images/01erc8t281bk9cddaarc6raher/utpalkumarsingh-jpg_1200x900.jpg)

  ## 2. हाल ही में L&T कंपनी को ब्रह्मपुत्रा नदी पर देश के सबसे बड़े सड़क पुल के निर्माण का कॉन्ट्रैक्ट मिला है
![Why India is worried about China's dam projects on the Brahmaputra river -  The Economic Times](https://m.economictimes.com/thumb/msid-54692522,width-1200,height-900,resizemode-4,imgsize-506574/news/politics-and-nation/why-india-is-worried-about-chinas-dam-projects-on-the-brahmaputra-river/why-india-should-worry-about-chinas-dam-projects-on-the-brahmaputra-river/why-india-is-worried-about-chinas-dam-on-brahmaputra-river.jpg)

  ## 3. हाल ही में अच्यूत सामंत को बॉलीबाल फेडरेशन का नया अध्यक्ष नियुत्क किया गया है
![Dr. Achyuta Samanta Wiki, Age, Wife, Family, Biography & More – WikiBio](https://225508-687545-raikfcquaxqncofqfm.stackpathdns.com/wp-content/uploads/2019/11/Achyuta-Samanta.jpg)


 ___
 # आज का सवाल 

 ### **जनसँख्या के अध्ययन को क्या कहते है ?**
![How population data can help countries plan and tweak policy](https://images.theconversation.com/files/286614/original/file-20190801-169696-2nvbgb.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=926&fit=clip)
