

  ## 1. हाल ही में प्रधानमंत्री नरेंद्र मोदी को 'लिजन ऑफ़ मेरिट' पुरस्कार से सम्मानित किया गया है
![Legion of Merit PM Narendra Modi Donald Trump India US strategic  partnership | India News – India TV](https://resize.indiatvnews.com/en/resize/newbucket/715_-/2020/12/legion-of-merit-1608606653.jpg)

  ## 2. हाल ही में फिजी देश में 'यासा' नामक चक्रवात आया है
![Fiji | History, Map, Flag, Points of Interest, & Facts | Britannica](https://cdn.britannica.com/40/183640-050-FE7992E5/World-Data-Locator-Map-Fiji.jpg)

  ## 3. हाल ही में खेल मंत्रालय ने खेलो इंडिया युथ गेम्स 2021 में 4 स्वदेशी खेलो को शामिल करने की घोषणा की है
![Khelo India Youth Games 2021: Sports Ministry Approves Inclusion Of 4  Indigenous Games | OTV News](https://img.odishatv.in/wp-content/uploads/2020/12/Indigenious-Games.jpg)

  ## 4. हाल ही में लुइस हैमिलटन ने 2020 का 'बीबीसी स्पोर्ट्स पर्सनालिटी ऑफ़ ईयर' पुरस्कार जीता है
![Lewis Hamilton crowned BBC Sports Personality of the Year 2020 | F1 News](https://e0.365dm.com/20/12/768x432/skysports-lewis-hamilton-formula-one_5213857.jpg?20201220185618)

  ## 5. हाल ही में उत्तर प्रदेश में भारत का पहला मेगा लेदर पार्क स्थापित किया जायेगा 
![India First Mega Leather Park In Kanpur - कानपुर में बनेगा देश का पहला मेगा  लेदर पार्क | Patrika News](https://new-img.patrika.com/upload/2020/12/17/photo_2020-12-17_16-31-49_6578624_835x547-m.jpg)


 ___
 # आज का सवाल 

 ### **दुर्गापुर लौह इस्पात किस देश की सहायता से स्थापित किया गया था ?**
![SAIL Durgapur awards SMS group with turnkey modernization for new  converters and environmental facilities | Press Detail | SMS group](https://www.sms-group.com/fileadmin/SMS_group_Website/Pictures/Press_and_media/Press_Releases/01_SAIL_Durgapur_05.06.2019.jpg)
