import React from 'react';

import {
  Container,
  Card,
} from 'react-bootstrap';

import ImgArmy from 'static/Army.jpg';
import ImgIAF1 from 'static/IAF1.jpg';
import ImgNavy from 'static/IndianNavy.jpg';
import ImgNavy1 from 'static/IndianNavy1.jpg';
import ImgBSF from 'static/BSF.webp';
import ImgITBP from 'static/ITBP.jpg';
import ImgCISF from 'static/CISF.jpg';
import ImgASR from 'static/ASR.jpg';
import ImgASR1 from 'static/ASR1.jpg';
import ImgNSG from 'static/NSG.jpg';

import HeroCarousel from 'Components/HeroCarousel';

import './styles.scss';

const Images = [
  {
    src: ImgArmy,
    title: "Indian Army",
  },
  {
    src: ImgIAF1,
    title: "Indian Air Force",
  },
  {
    src: ImgNavy1,
    title: "Indian Navy",
  },
  {
    src: ImgNavy,
    title: "Indian Navy",
  },
  {
    src: ImgBSF,
    title: "Border Security Force",
  },
  {
    src: ImgITBP,
    title: "Indo-Tibetan Border Police",
  },
  {
    src: ImgCISF,
    title: "Central Industrial Security Force",
  },
  {
    src: ImgASR,
    title: "Assam Rifles",
  },
  {
    src: ImgASR1,
    title: "Assam Rifles",
  },
  {
    src: ImgNSG,
    title: "National Security Guard",
  },
];

const Home= () => {
  return (
    <div className="home-page">
      <HeroCarousel
        Images={Images}
      />
      <Container className="mt-5 quoteCard">
      <Card>
        <Card.Header><h2><span role="img" aria-label="notes emoji">️📋</span> Time Table</h2></Card.Header>
        <Card.Body>
          <Card.Text>
            <ul>
              <li>
                रोजाना शाम 7:00 बजे करंट अफेयर्स
              </li>
              <li>
                रोजाना शाम 7:00 बजे एक प्रश्न पूरे विस्तार के साथ
              </li>
              <li>
                हर रविवार शाम 8:00 बजे ऑनलाइन परीक्षा
              </li>
              <li>
                टॉप 100 मासिक करंट अफेयर्स
              </li>
            </ul>
          </Card.Text>
        </Card.Body>
      </Card>
      </Container>
    </div>
  );
}

export default Home;
