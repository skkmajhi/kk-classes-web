import React from 'react';
import {
  Row,
  Col,
  Container,
} from 'react-bootstrap';

import Avatar from 'Components/Avatar';
import './styles.scss';

const TEACHERS = [
  {
    name: 'KK Yadav',
    src: 'https://www.whizsky.com/wp-content/uploads/2014/12/dummy-user.jpg',
    subject: 'Fundamental Law, UP Special, GK, Current Affairs'
  },
  {
    name: 'Phulbachan Prajapati',
    src: 'https://www.whizsky.com/wp-content/uploads/2014/12/dummy-user.jpg',
    subject: 'Mathematics'
  },
  {
    name: 'Diwakar Yadav',
    src: 'https://www.whizsky.com/wp-content/uploads/2014/12/dummy-user.jpg',
    subject: 'Reasoning'
  },
  {
    name: '-',
    src: 'https://www.whizsky.com/wp-content/uploads/2014/12/dummy-user.jpg',
    subject: 'Hindi'
  },
];

const AboutUs= () => {
  return (
      <Container>
        <div className="profile">
          <Row>
            <Col className="director">
              <Avatar
                name="KK Yadav"
                subject="Founder and Faculty"
                src="https://www.whizsky.com/wp-content/uploads/2014/12/dummy-user.jpg"
              />
            </Col>
          </Row>
          <Row>
            <Col className="director">
              <Avatar
                name="Ahmad Ali (Guddu)"
                subject="Office Manager"
                src="https://www.whizsky.com/wp-content/uploads/2014/12/dummy-user.jpg"
              />
            </Col>
          </Row>
          <h2>Faculty</h2>
          <hr/>
          <div className="teachers">
          { TEACHERS.map(
              teacher => (
                <Avatar
                  name={teacher.name}
                  subject={teacher.subject}
                  src={teacher.src}
                />
              )
            )}
          </div>
        </div>
      </Container>
  )
}

export default AboutUs;
